<?php

/**
 * Widgets Controller
 * 
 * @property Widget $Widget 
 */

class WidgetsController extends DashboardManagerAppController {
    
    public function index(){
        App::uses('Folder','Utility');
        $widgetDir = new Folder(APP.DS.'View'.DS.'Elements'.DS.'widgets');
        $this->set('widgets',$widgetDir->find('.*\.ctp'));
    }
    
    public function add(){
        if ($this->request->is('post')) {
            $this->Widget->create();
            if ($this->Widget->save($this->request->data)) {
                $this->Session->setFlash(__('Widget added to dashboard.'),'default',array('class'=>'success'));
                $this->redirect(array('action'=>'index'));
            }
        }
        App::uses('Folder','Utility');
        $widgetDir = new Folder(APP.DS.'View'.DS.'Elements'.DS.'widgets');
        $widgets = $widgetDir->find('.*\.ctp');
        $dashboards = $this->Widget->Dashboard->find('list',array('conditions'=>array('Dashboard.user_id' => AuthComponent::user('id'))));
        $this->set(compact('widgets','dashboards'));
    }
}

?>
