<?php
App::uses('AppController','Controller');
/**
 * Dashboard Controller
 * 
 * @property Dashboard $Dashboard 
 */
class DashboardsController extends DashboardManagerAppController {
    
    public function index(){
        $conditions = array(
            'Dashboard.user_id' => $this->Session->read('Auth.User.id')
        );
        if ($this->request->requested) {
            $this->Dashboard->recursive = 1;
            $conditions['Dashboard.active'] = 1;
            return $this->Dashboard->find('list',array(
                'conditions' => $conditions
            ));
        }
        $this->paginate = array('conditions' => $conditions);
        $this->set('dashboards',$this->paginate());
    }
    
    public function view($id = null) {
        $this->Dashboard->id = $id;
        if (!$this->Dashboard->exists()) {
            throw new NotFoundException(__('Invalid Dashboard'));
        }
        $this->helpers[] = 'Widget';
        if ($this->request->requested) {
            $this->Dashboard->recursive = 1;
            return $this->Dashboard->read(null, $id);
        }
        $dashboard = $this->Dashboard->read(null, $id);
        $this->set('dashboard', $dashboard);
    
    }


    public function add(){
        if($this->request->is('post')){
            $this->Dashboard->create();
            if ($this->Session->read('Auth')) {
                $this->request->data['DashboardManager']['Dashboard']['user_id'] = $this->Session->read('Auth.User.id');
            }else{
                $this->request->data['DashboardManager']['Dashboard']['user_id'] = 0;
            }
            if ($this->Dashboard->save($this->request->data['DashboardManager'])) {
                $this->Session->setFlash(__('Dashboard has been created.'),'default',array('class'=>'success'));
                $this->redirect(array('action'=>'index'));
            }else{
                $this->Session->setFlash(__('Dashboard could not be saved, please try again.'),'default',array('class'=>'error-message'));
            }
        }
        $users = $this->Dashboard->User->find('list');
        $this->set(compact('users'));
    }
    
    public function layout($id = null, $layout = null){
        $this->Dashboard->id = $id;
        if (!$this->Dashboard->exists()) {
            $return = array('message' => 'error');
        }
        $dashboardData['Dashboard']['id'] = $id;
        $dashboardData['Dashboard']['layout'] = $layout;
        if ($this->Dashboard->save($dashboardData)) {
            $return = array(
                'message' => 'success',
                'layout'  => $layout
                );
        }else{
            $return = array('message' => 'error');
        }
        $this->set('return', $return);
        $this->set('_serialize', array('return'));
    }
    
    public function admin_index(){
        if ($this->request->requested) {
            $this->Dashboard->recursive = 1;
            return $this->Dashboard->find('list',array(
                'limit' => 10,
                'conditions' => array(
                    'Dashboard.user_id' => $this->Session->read('Auth.User.id'),
                    'Dashboard.active' => 1
                )
            ));
        }
        $this->set('dashboards',$this->paginate());
    }
    
    public function admin_view($id = null) {
        $this->Dashboard->id = $id;
        if (!$this->Dashboard->exists()) {
            throw new NotFoundException(__('Invalid Dashboard'));
        }
        $this->helpers[] = 'Widget';
        if ($this->request->requested) {
            $this->Dashboard->recursive = 1;
            return $this->Dashboard->read(null, $id);
        }
        $dashboard = $this->Dashboard->read(null, $id);
        $this->set('dashboard', $dashboard);
    
    }


    public function admin_add(){
        if($this->request->is('post')){
            $this->Dashboard->create();
            if ($this->Session->read('Auth')) {
                $this->request->data['DashboardManager']['Dashboard']['user_id'] = $this->Session->read('Auth.User.id');
            }else{
                $this->request->data['DashboardManager']['Dashboard']['user_id'] = 0;
            }
            if ($this->Dashboard->save($this->request->data['DashboardManager'])) {
                $this->Session->setFlash(__('Dashboard has been created.'),'default',array('class'=>'success'));
                $this->redirect(array('action'=>'index'));
            }else{
                $this->Session->setFlash(__('Dashboard could not be saved, please try again.'),'default',array('class'=>'error-message'));
            }
        }
        $users = $this->Dashboard->User->find('list');
        $this->set(compact('users'));
    }
}

?>
