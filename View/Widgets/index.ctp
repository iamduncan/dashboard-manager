<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('File','Utility');
?>
<div class="index">
<h2><?php echo __('Widgets Available'); ?></h2>
<?php foreach ($widgets as $key => $name){
    echo "<div class=\"widget_info\">";
    $widgetName = explode('.', $name);
    $widgetList[] = $widgetName[0];
    echo '<h3>'.ucwords($widgetName[0]).'</h3>';
    $file = new File(APP.DS.'View'.DS.'Elements'.DS.'widgets'.DS.$name);
    $source = $file->read();
    $file->close();
    $tokens = token_get_all($source);
    $comment = array(
        T_COMMENT,
        T_DOC_COMMENT
    );
    $titles = array(
        'Widget Name',
        'Description',
        'Version',
        'Author',
        'Date'
    );
    foreach( $tokens as $token ) {
        if( !in_array($token[0], $comment) ){
            
        }else{
            // Do something with the comment
            $txt = $token[1];
            $txt = str_replace("\n */", "", $txt);
            $txt = str_replace("/*\r\n", "", $txt);
            $txt = str_replace("/*\n", "", $txt);
            $txt = str_replace(" * ", "", $txt);
            $lines = explode("\n", $txt);
            break;
        }
    }
    if (!empty($lines)) {
        
    foreach($lines as $line){
        $line_data = explode(":",$line);
        if(in_array($line_data[0],$titles)){
            $data[$line_data[0]] = $line_data[1];
        }
    }
    if(isset($data)){ ?>
    <dl>
        <dt><?php echo __('Name'); ?></dt>
        <dd><?php echo $data['Widget Name']; ?>&nbsp;</dd>
        <dt><?php echo __('Description'); ?></dt>
        <dd><?php echo $data['Description']; ?>&nbsp;</dd>
        <dt><?php echo __('Version'); ?></dt>
        <dd><?php if(isset($data['Version'])) echo $data['Version']; ?>&nbsp;</dd>
        <dt><?php echo __('Author'); ?></dt>
        <dd><?php if(isset($data['Author'])) echo $data['Author']; ?>&nbsp;</dd>
        <dt><?php echo __('Date'); ?></dt>
        <dd><?php if(isset($data['Date'])) echo $data['Date']; ?>&nbsp;</dd>
    </dl>
<?php
    }else {
        echo __('No Data Found.');
    }
}else {
    echo __('No Data Found.');
}
echo "</div>";
unset($lines);
unset($data);
};
?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Add Widget'), array('controller' => 'widgets', 'action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Dashboards'), array('controller' => 'dashboards', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Add Dashboard'), array('controller' => 'dashboards', 'action' => 'add')); ?></li>
    </ul>
</div>