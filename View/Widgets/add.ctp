<div class="form">
    <?php foreach ($widgets as $key => $name){
        $widgetName = explode('.', ucwords($name));
        $widgetList[$widgetName[0]] = $widgetName[0];
    } ?>
    <?php echo $this->Form->create('Widget'); ?>
    <?php echo $this->Form->input('Widget.name',array('options'=>$widgetList)); ?>
    <?php echo $this->Form->input('Widget.dashboard_id'); ?>
    <?php echo $this->Form->input('Widget.data'); ?>
    <?php echo $this->Form->end(__('Add')); ?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('List Widgets'), array('controller' => 'widgets', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Dashboards'), array('controller' => 'dashboards', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Add Dashboard'), array('controller' => 'dashboards', 'action' => 'add')); ?></li>
    </ul>
</div>