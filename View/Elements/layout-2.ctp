<div class="yui3-g">
    <div class="yui3-u-2-3">
    <?php
        foreach($widgets as $widget){
            if ($widget['column'] == 0) {
                echo $this->Widget->show(strtolower($widget['name']),array(
                    'title' => $widget['name']
                ));
            }
        }
    ?>
    </div>
    <div class="yui3-u-1-3">
    <?php
        foreach($widgets as $widget){
            if ($widget['column'] == 1) {
                echo $this->Widget->show(strtolower($widget['name']),array(
                    'title' => $widget['name']
                ));
            }
        }
    ?>        
    </div>
</div>
