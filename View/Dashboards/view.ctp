<div class="view">
    <h2><?php echo $dashboard['Dashboard']['name']; ?></h2>
    <h3><?php echo __('Widgets'); ?></h3>
<?php
//pr($dashboard);
foreach ($dashboard['Widgets'] as $widget):
?>
    <div class="widget">
        <strong><?php echo $widget['name']; ?></strong>
        <?php pr($widget['data']); ?>
    </div>
<?php
endforeach;
?>
    <div id="layoutSelection">
        <h3><?php echo __('Layout'); ?></h3>
        <input type="image" src="/img/icons/1-column-layout.png" alt="" class="yui3-button<?php if ($dashboard['Dashboard']['layout'] === '0') echo ' layout-selected'; ?>" id="layout-0" />
        <input type="image" src="/img/icons/2-column-left-narrow-layout.png" alt="" class="yui3-button<?php if ($dashboard['Dashboard']['layout'] === '1') echo ' layout-selected'; ?>" id="layout-1" />
        <input type="image" src="/img/icons/2-column-right-narrow-layout.png" alt="" class="yui3-button<?php if ($dashboard['Dashboard']['layout'] === '2') echo ' layout-selected'; ?>" id="layout-2" />
    </div>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Edit'), array('controller' => 'dashboards', 'action' => 'edit',$dashboard['Dashboard']['id'])); ?></li>
        <li><?php echo $this->Html->link(__('Add Widget'), array('controller' => 'widgets', 'action' => 'add')); ?></li>
    </ul>
</div>
<script type="text/javascript">
    YUI().use('button-plugin','io-base', 'json-parse',function(Y){
        
        var inputs = Y.all('#layoutSelection input');
        
        function callback(id, o, args){
            var data = Y.JSON.parse(o.responseText);
            inputs.removeClass('layout-selected');
            inputs.removeClass('yui3-button-disabled');
            Y.one('#layout-' + data['return']['layout']).addClass('layout-selected');
        }
        
        Y.on('io:start',function(){
            inputs.addClass('yui3-button-disabled');
        },Y);
        Y.on('io:complete',callback,Y);
        
        var layout0Button = Y.one('#layout-0');
        layout0Button.plug(Y.Plugin.Button);
        layout0Button.on("click",function(){
            Y.io('/dashboard_manager/dashboards/layout/<?php echo $dashboard['Dashboard']['id']; ?>/0.json');
        });
        var layout1Button = Y.one('#layout-1');
        layout1Button.plug(Y.Plugin.Button);
        layout1Button.on("click",function(){
            Y.io('/dashboard_manager/dashboards/layout/<?php echo $dashboard['Dashboard']['id']; ?>/1.json');
        });
        var layout2Button = Y.one('#layout-2');
        layout2Button.plug(Y.Plugin.Button);
        layout2Button.on("click",function(){
            Y.io('/dashboard_manager/dashboards/layout/<?php echo $dashboard['Dashboard']['id']; ?>/2.json');
        });
    });
</script>