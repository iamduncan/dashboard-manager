<?php

?>
<div class="form">
    <?php echo $this->Form->create('DashboardManager.Dashboard'); ?>
    <?php echo $this->Form->inputs(array(
        'name', 'active',
        'legend' => __('Create New Dashboard')
    )); ?>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>