<div class="index">
    <h2><?php echo __('Dashboards'); ?></h2>
    <table>
        <tr>
            <th><?php echo __('Name'); ?></th>
            <th><?php echo __('User'); ?></th>
            <th><?php echo __('Widgets'); ?></th>
            <th>Actions</th>
        </tr>
    <?php foreach ($dashboards as $key => $dashboard): ?>
        <tr>
            <td><?php echo $dashboard['Dashboard']['name']; ?></td>
            <td><?php echo $dashboard['User']['username']; ?></td>
            <td><?php echo count($dashboard['Widgets']); ?></td>
            <td class="actions">
                <?php echo $this->Html->link(__('View'), array('controller' => 'dashboards', 'action' => 'view',$dashboard['Dashboard']['id'])); ?>
                <?php echo $this->Html->link(__('Edit'), array('controller' => 'dashboards', 'action' => 'edit',$dashboard['Dashboard']['id'])); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Add Dashboard'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Widgets'), array('controller' => 'widgets', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Add Widget'), array('controller' => 'widgets', 'action' => 'add')); ?></li>
    </ul>
</div>