<?php

class Dashboard extends DashboardManagerAppModel {
    
    public $useTable = 'dashboard_manager_dashboards';
    
    public $belongsTo = array(
        'User'
    );
    
    public $hasMany = array(
        'Widgets' => array(
            'className' => 'DashboardManager.Widget',
            'foreignKey' => 'dashboard_id'
        )
    );
}
?>
