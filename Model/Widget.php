<?php

class Widget extends DashboardManagerAppModel {
    
    public $useTable = 'dashboard_manager_widgets';
    
    public $belongsTo = array(
        'DashboardManager.Dashboard'
    );
}
?>
